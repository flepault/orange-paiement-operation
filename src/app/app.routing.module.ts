import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExportClientComponent } from './layout/export-client/export-client.component';
import { CancelDebtComponent } from './layout/cancel-debt/cancel-debt.component';

const homeRoute = ''
const exportRoute = 'export';
const importRoute = 'import';

const routes: Routes = [
    {path: homeRoute, component: ExportClientComponent},
    {path: exportRoute, component: ExportClientComponent},
    {path: importRoute, component: CancelDebtComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
