import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import * as XLSX from 'xlsx';
import { CanceldebtService } from '../../shared/service/canceldebt/canceldebt.service';

@Component({
    selector: 'app-cancel-debt',
    templateUrl: './cancel-debt.component.html',
    styleUrls: ['./cancel-debt.component.css']
})
export class CancelDebtComponent implements OnInit {

    public isMobile$: Observable<boolean>;

    public worksheet: any = undefined;

    public cancelDebtOK = false;

    public errorMessage: string = undefined;

    constructor(private canceldebtService: CanceldebtService,
                private responsiveService: ResponsiveService) {
    }

    ngOnInit() {
        this.isMobile$ = this.responsiveService.isMobile();
    }

    uploadedFile(event) {
        this.cancelDebtOK = false;
        this.errorMessage = undefined;
        this.readAsExcelFile(event.target.files[0]);
    }

    public onClick(): void {
        this.cancelDebtOK = false;
        this.errorMessage = undefined;

        this.canceldebtService.cancelDebt(XLSX.utils.sheet_to_json(this.worksheet, {raw: false})).subscribe(
            value => {
                console.log(value);
                this.worksheet = undefined;
                this.cancelDebtOK = true;

            }, error => {
                this.errorMessage = error;
                console.log(error);
            }
        );
    }

    public readAsExcelFile(file: File) {

        const readFile = new FileReader();
        readFile.onload = (e) => {
            const storeData: any = readFile.result;
            const data = new Uint8Array(storeData);
            const arr = [];
            for (let i = 0; i !== data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            const bstr = arr.join('');
            const workbook = XLSX.read(bstr, {type: 'binary'});
            const firstSheetName = workbook.SheetNames[0];
            this.worksheet = workbook.Sheets[firstSheetName];
        }
        readFile.readAsArrayBuffer(file);

    }
}
