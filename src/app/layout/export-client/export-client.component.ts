import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { ExtractService } from '../../shared/service/extract/extract.service';
import { ExcelService } from '../../shared/service/excel/excel.service';
import { Customer } from '../../shared/model/customer.model';
import { DatePipe } from "@angular/common";
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';

const moment = _moment;

export const MY_FORMATS = {
    parse: {
        dateInput: ['DD/MM/YYYY', 'DD-MM-YYYY', 'DD/MM/YYYY', 'DD MM YYYY']
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-export-client',
    templateUrl: './export-client.component.html',
    styleUrls: ['./export-client.component.css'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
})
export class ExportClientComponent implements OnInit {

    public exportClientForm: FormGroup;

    public isMobile$: Observable<boolean>;

    constructor(private responsiveService: ResponsiveService,
                private extractService: ExtractService,
                private excelService: ExcelService,
                private datepipe: DatePipe) {
        this.exportClientForm = new FormGroup({
            montant: new FormControl('', [Validators.required, Validators.min(1)]),
            date: new FormControl(moment(), Validators.required)
        });
    }

    ngOnInit() {
        this.isMobile$ = this.responsiveService.isMobile();
    }

    public exporter(): void {
        if (this.exportClientForm.valid) {
            console.log('Generation fichier Excel...');
            this.extractService.extract(
                this.exportClientForm.controls.montant.value,
                this.datepipe.transform(this.exportClientForm.controls.date.value,'ddMMyyyy')).subscribe((customers: Customer[]) => {

                this.excelService.exportAsExcelFile(customers, 'Extraction_clients')

            });
        }
    }
}
