import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CancelDebtComponent } from './cancel-debt/cancel-debt.component';
import { ExportClientComponent } from './export-client/export-client.component';

@NgModule({
    imports: [SharedModule],
    declarations: [CancelDebtComponent, ExportClientComponent],
    exports: [CancelDebtComponent, ExportClientComponent]
})
export class LayoutModule {
}
