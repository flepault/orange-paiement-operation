export interface Customer {
    customerId: number;
    ohxact: number;
    amount: number;
    cachkNum: string;
}