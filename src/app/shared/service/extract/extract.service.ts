import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Customer } from '../../model/customer.model';

@Injectable({
    providedIn: 'root'
})
export class ExtractService {

    constructor(private http: HttpClient) {
    }

    // UPLOAD IMAGE GALLERIE
    public extract(montant: string, date: string): Observable<Customer[]> {
        return this.http.get<Customer[]>(environment.extractURL + '/api/customer/export', {
            params: {
                montant,
                date
            }
        });
    }

}