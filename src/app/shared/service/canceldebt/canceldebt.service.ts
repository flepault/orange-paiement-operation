import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Customer } from '../../model/customer.model';

@Injectable({
    providedIn: 'root'
})
export class CanceldebtService {

    constructor(private http: HttpClient) {
    }

    // UPLOAD FILE
    public cancelDebt(customers: Customer[]): Observable<Customer[]> {
        return this.http.post<Customer[]>(environment.canceldebtURL + '/api/customer/canceldebt',
            customers);
    }


}
